// Johnathan Burns
// 2018-07-19
// Flux Capacitor Audio/LED code

#include <Adafruit_Soundboard.h>

// I/O pins
#define LED_ZONE_1_1 5
#define LED_ZONE_1_2 6
#define LED_ZONE_2_1 9
#define LED_ZONE_2_2 10
#define LED_ZONE_3_1 11
#define LED_ZONE_3_2 12
#define SFX_RESET 2
#define MOTION_SENSOR 3
#define SOUND_ENABLE 13

// Constants for LED display
// The below values have been chosen to ensure 30 seconds of flashing
// 180 ms/flash * 167 flashes = 30 060 ms, or 30.06 s of flashing
#define FLASH_DELAY 180         // The amount of time between LED flashes
#define NUMBER_OF_FLASHES 167   // The number of times to flash the LEDs
#define NUMBER_OF_SOUND_FILES 5 // The number of sound files on the SFX board

// Adafruit Soundboard setup
Adafruit_Soundboard adafruit_soundboard = Adafruit_Soundboard(&Serial1, NULL, SFX_RESET);

// Global Variables
bool motion_sensed = false;

void setup()
{
  // Initialize Serial communications with the computer (if present) and Adafruit Soundboard
  Serial.begin(9600);
  Serial1.begin(9600);

  // Setup IO Pins
  pinMode(MOTION_SENSOR, INPUT);
  pinMode(SOUND_ENABLE, INPUT);
  pinMode(LED_ZONE_1_1, OUTPUT);
  pinMode(LED_ZONE_1_2, OUTPUT);
  pinMode(LED_ZONE_2_1, OUTPUT);
  pinMode(LED_ZONE_2_2, OUTPUT);
  pinMode(LED_ZONE_3_1, OUTPUT);
  pinMode(LED_ZONE_3_2, OUTPUT);

  // Ensure all the LEDs are turned off
  digitalWrite(LED_ZONE_1_1, LOW);
  digitalWrite(LED_ZONE_1_2, LOW);
  digitalWrite(LED_ZONE_2_1, LOW);
  digitalWrite(LED_ZONE_2_2, LOW);
  digitalWrite(LED_ZONE_3_1, LOW);
  digitalWrite(LED_ZONE_3_2, LOW);

  // Test the Adafruit Soundboard
  if (!adafruit_soundboard.reset())
  {
    Serial.println("Sound board not found. Please connect the board properly and reset.");
    while(1);
  }
  Serial.println("Sound board connected.");
}

// Runs continuously to check for updates to the motion sensor and play audio if necessary
void loop()
{
  delay(100);

  // Check if the motion sensor has found new motion
  if ((digitalRead(MOTION_SENSOR) == HIGH) && !(motion_sensed))
  {
    Serial.println("Motion detected.");
    motionSensorInterrupt();
    motion_sensed = true;
  }
  // Reset the motion sensor variables if no motion is detected
  else if((digitalRead(MOTION_SENSOR) == LOW) && motion_sensed)
  {
    motion_sensed = false;
    Serial.println("Motion no longer detected.");
  }

  // Make the audio chip behave
  flushInput();
}

// Function called when the motion sensor reads new motion
void motionSensorInterrupt()
{
  // Only plays music if sound is enabled via the sound enable switch
  if (digitalRead(SOUND_ENABLE) == LOW)
  {
    // Picks a random track from the possible choices
    uint8_t n = random(0, NUMBER_OF_SOUND_FILES);
    Serial.println("Playing track with ID: " + n);
    // Tries to play the track, and writes a debug message to the serial console if that fails
    if (! adafruit_soundboard.playTrack(n) )
    {
      Serial.println("Failed to play track?");
    }
  }
  // Make the LED's flash
  blinkLEDs();
}

// Function from the Adafruit Soundboard Example
void flushInput()
{
  // Read all available serial input to flush pending data.
  uint16_t timeoutloop = 0;
  while (timeoutloop++ < 40)
  {
    while(Serial1.available())
    {
      Serial1.read();
      timeoutloop = 0;  // If char was received reset the timer
    }
    delay(1);
  }
}

// Function to blink the LEDs
void blinkLEDs()
{
  // This code runs NUMBER_OF_FLASHES times, blinking the proper LEDs each time
  for( int i = 0; i < NUMBER_OF_FLASHES; i++)
  {
    // Activate the inner LEDs
    digitalWrite(LED_ZONE_1_1, HIGH);
    digitalWrite(LED_ZONE_2_1, HIGH);
    digitalWrite(LED_ZONE_3_1, HIGH);
    delay(FLASH_DELAY/2);
    digitalWrite(LED_ZONE_1_1, LOW);
    digitalWrite(LED_ZONE_2_1, LOW);
    digitalWrite(LED_ZONE_3_1, LOW);

    // Activate the outer LEDs
    digitalWrite(LED_ZONE_1_2, HIGH);
    digitalWrite(LED_ZONE_2_2, HIGH);
    digitalWrite(LED_ZONE_3_2, HIGH);
    delay(FLASH_DELAY/2);
    digitalWrite(LED_ZONE_1_2, LOW);
    digitalWrite(LED_ZONE_2_2, LOW);
    digitalWrite(LED_ZONE_3_2, LOW);
  }
}

